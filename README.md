# Repository for Post Domain Adaptation

## Todo List

1. Download imagenette & cifar10 data.

2. Style transfer data, i.e., day/night, rain/snow/sun.

3. Data directories structure:
    ```
    .
    └── imagenette_6_scenes
        ├── scene1
        │   ├── train
        │   └── val
        ├── scene2
        │   ├── train
        │   └── val
        ├── scene3
        │   ├── train
        │   └── val
        ├── scene4
        │   ├── train
        │   └── val
        ├── scene5
        │   ├── train
        │   └── val
        └── scene6
            ├── train
            └── val
    ```

4. Template code:

```python
class Data:
    def __init__(self, root):
        self.scenes = self.get_scenes(root)
        pass

    def get_scenes(self, root):
       pass


class Model:
    def add_data(self,):
       pass

    def make_block_copy(self):
       pass

    def switch_forward_data(self):
       pass

    def num_paths(self):
       pass


data = Data()
model = Model()
model.add_data(data)

for i in range(model.num_paths()):
    dataset = model.switch_forward_data(i)

    model.fit(dataset)

```
